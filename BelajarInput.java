import java.util.Scanner;
 
public class BelajarInput {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Masukkan angka tertentu : ");
        float angkaDariUser = scanner.nextFloat();
        String tulisanUser = scanner.nextLine();
       
        System.out.println("Nilainya adalah " + angkaDariUser);
        System.out.println("Tulisannya adalah " + tulisanUser);
        scanner.close();    
    }
}
