public class BelajarVariable {
    public static void main(String[] args) {

        int tanggalLahir = 27;
        long tigaRatusMiliar = 3000000000000L;
        float sepuluhDibagTiga = 3.33f;
        boolean iniBenar = true;
        char hurufA = 65;
        String helloWorld = "Hello World";

        System.out.println(tanggalLahir);
        System.out.println(tigaRatusMiliar);
        System.out.println(sepuluhDibagTiga);
        System.out.println(iniBenar);
        System.out.println(hurufA);
        System.out.println(helloWorld.toUpperCase());

    }
}
