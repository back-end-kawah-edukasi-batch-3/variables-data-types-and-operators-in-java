public class BelajarOperasi {
    public static void main(String[] args) {
        
        int nilaiSeratus = 100;
        int angkaAcak = 15;

        System.out.println(100 + 50);
        System.out.println(100 - 50);
        System.out.println(nilaiSeratus - 50);
        System.out.println(nilaiSeratus * 8);
        System.out.println(nilaiSeratus / 5);
        System.out.println(nilaiSeratus % 30);
        System.out.println(Math.pow(100, 2));
        System.out.println(++nilaiSeratus);
        System.out.println(--nilaiSeratus);
        System.out.println(nilaiSeratus += 3);
        System.out.println(nilaiSeratus == 100);
        System.out.println(nilaiSeratus > 100);
        System.out.println(angkaAcak & 3);




    }
}
